;; configure package.el to include MELPA

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.


(require 'package)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)

(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))


(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
(org-babel-load-file "~/.emacs.d/zc.org")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(zc))
 '(custom-safe-themes
   '("a4b1c4e0867e1f04a7d37ce0bed76325632533f7a71ded58bf8d2c7ab41530ff" default))
 '(package-selected-packages
   '(neotree sphinx-doc python-mode htmlize ein yasnippet use-package undo-tree sage-shell-mode pylint projectile mutt-mode multiple-cursors magit lua-mode flycheck))
 '(whitespace-style
   '(face trailing tabs empty indentation space-after-tab::tab space-after-tab::space space-after-tab space-before-tab space-mark tab-mark)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mode-line ((t (:background "black" :foreground "color-130" :box (:line-width -1 :style released-button)))))
 '(mode-line-inactive ((t (:inherit mode-line :background "color-16" :foreground "color-236" :box (:line-width -1 :color "grey40") :weight light))))
 '(whitespace-tab ((t (:foreground "green")))))
