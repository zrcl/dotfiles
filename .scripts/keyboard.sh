#!/bin/sh
layout=$(setxkbmap -query | grep -o -P "(?<=layout: ).*")
variant=$(setxkbmap -query | grep -o -P "(?<=variant: ).*")
echo $variant
[[ $layout == *"us"* ]] && [[ $variant == "" ]] && new="us -variant intl"
[[ $layout == *"us"* ]] && [[ $variant == *"intl"* ]] && new="fr"
[[ $layout == *"fr"* ]] && new="us"
setxkbmap -layout $new
~/.dotfiles/.scripts/refBar.sh
#kill "$(pstree -lp | grep -- -bar.sh\([0-9] | sed "s/.*sleep(\([0-9]\+\)).*/\1/")"
