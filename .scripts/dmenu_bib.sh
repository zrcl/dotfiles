#!/bin/bash

# select a bib file from my main bib folder
bibfile=$(ls $BIB/*.bib | dmenu -l 10)

# chose an item in the bib file then cut the string
chosen=$(grep -E '*(@article|@book)|title' $bibfile | awk 'NR%2{printf "%s ",$0;next;}1' | dmenu -l 10 -i | grep -o -P '(?<=article{).*(?=, )')

# clip the label, paste it with the wheel clic or with maj+insert
echo -n $chosen | xclip
