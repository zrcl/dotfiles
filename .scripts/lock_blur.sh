im=/tmp/i3lock.png
screenshot="scrot $im"

# depending on your resolution and number of monitor this might take a few sec (2~3s)
blurrez="2x8" 

$screenshot
convert $im -grayscale Rec709Luma $im
convert $im -blur $blurrez $im
i3lock -u -i $im
rm $im
