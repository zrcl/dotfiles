#!/bin/sh
status() { \
	   #keyboard
	   layout=$(setxkbmap -query | grep -o -P "(?<=layout: ).*")
	   variant=$(setxkbmap -query | grep -o -P "(?<=variant: ).*")
	   #sound
	   status=$(pulsemixer --get-mute)
	   volume=($(pulsemixer --get-volume))
	   [ $status = 1 ] && sound="audio: mute"
	   [ $status = 0 ] && sound="audio: $volume%"
	   #date
	   d=$(date '+%d %b %y - %I:%M:%p')
	   #battery level
	   b=$(cat /sys/class/power_supply/BAT0/capacity)
           #battery plugged
           plugged=$(cat /sys/class/power_supply/AC/online)
           if [ $plugged -eq 1 ]
           then
               plugStatus="Plugged"
           else
               plugStatus=""
           fi
	   #ethernet
	   e=$(cat /sys/class/net/enp0s31f6/operstate)
	   #wifi
	   w=$(cat /sys/class/net/wlp2s0/operstate)
	   wn=$(iw dev | grep -o -P "(?<=ssid ).*")
	   #memory
           freeMem=$(free -h | awk '/^Mem:/ {print $3 "/" $2}')
           #temp
           temp=$(sensors | awk '/^Package id 0/ {print $4}')
	   # to be done with something like grep -o -P "(?<=MemFree:).*" /proc/meminfo
	   xsetroot -name "$sound | kbd: $layout$variant | mem: $freeMem | temp: $temp | eth: $e | wifi: $w $wn | bat: $b% $plugStatus | $d"
           if [ $b -lt 10 ]
           then
               if [ $plugged -eq 0 ]
               then
                  herbe "Battery running low"
               fi
           fi
}

while :; do
    status
    sleep 10s
done
