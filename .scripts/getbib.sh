# Taken from a video of Luke Smith
# modified to handle poorly written dois

[ -z "$1" ] && echo "Give either a pdf or a DOI as an argument." && exit

if [ -f "$1" ]; then
    # try to get the doi from pdfinfo of pdftotext
    doi=$(pdfinfo "$1" | grep -io "doi:.*") ||
	doi=$(pdftotext "$1" 2>/dev/null - | grep -io "doi:.*" -m 1) ||
	exit 1
else
    doi="$1"
fi
doi=$(echo ${doi//[[:blank:]]/}) #remove spaces
doi=$(echo ${doi//)}) #remove trailing parenthesis
#send request to cross ref
curl -s "https://api.crossref.org/works/$doi/transform/application/x-bibtex" -w "\\n"
	
