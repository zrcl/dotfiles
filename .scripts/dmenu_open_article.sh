#!/bin/bash

# chose bib folder
folder=$(ls -d $PAPERS/*/ | dmenu -i -l 10)
# chose pdf then open with evince only if a choice was made
chosen=$(ls $folder*.pdf | dmenu -i -l 10) && zathura $chosen

