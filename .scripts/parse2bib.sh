#!/bin/bash
#  _____ _____ _____ _____ _____ _____ _____ __    
# |__   |   __| __  |     |     |     |     |  |   
# |   __|   __|    -|  |  |   --|  |  |  |  |  |__ 
# |_____|_____|__|__|_____|_____|_____|_____|_____|
# 2020 November 10.


# parse file to get all \cite{..} occurences
# -o gets rif of the line, outputing only the match itself
# -P uses perl regex
# h gets rid of paths if a list of files is given
# \K deletes firts delimter, didn't find how to delete trailing } unsing grep, so i pipe it to tr
# pipe it to tr a few times to isolate bib tags
listTags=$(grep -oPh '\\cite{\K\S*?}' "$@" | tr ',' '\n' | tr -d '}' )
#sort and remove duplicates
listTags=$(echo $listTags | tr ' ' '\n' | sort -k2 | uniq)
#--------------
#---Print infos
#--------------
#count references and print informations
count=$(echo $listTags | wc -w)
echo 'I have found ' $count ' references'
echo 'Tags are :'
for tag in $listTags
do
    echo -e '\t' $tag
done
#locate in general bibfile
for tag in $listTags
do
    a=$(grep $tag $BIB || echo 'didnt find ' $tag)
    sed -n "/^$a/,/^}/p" $BIB | tr ',' ',\n' >> refs.bib
done
